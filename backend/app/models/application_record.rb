# frozen_string_literal: true

# Application level active record model
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
