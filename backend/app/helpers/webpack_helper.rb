# frozen_string_literal: true

# Webpack manifest helper
module WebpackHelper
  def javascript_pack(asset_name)
    content_tag(:script, nil, src: asset_pack_path("#{asset_name}.js"))
  end

  def asset_pack_path(asset_name)
    manifest_path = Rails.root.join('public', 'assets', 'manifest.json')
    manifest = JSON.parse(manifest_path.read)
    compiled_asset_name = manifest[asset_name]
    asset_web_path = ['/assets', compiled_asset_name].join('/')
    asset_web_path
  end
end
