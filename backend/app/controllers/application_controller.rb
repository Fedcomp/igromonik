# frozen_string_literal: true

# Base application controller class
class ApplicationController < ActionController::Base
  def index; end
end
