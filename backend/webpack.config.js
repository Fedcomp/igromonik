const path = require('path')
const ManifestPlugin = require('webpack-manifest-plugin')

module.exports = {
  mode: 'development',
  entry: {
    application: './app/assets/javascripts/application.js'
  },
  output: {
    path: path.resolve(__dirname, 'public', 'assets'),
    filename: '[name].bundle.[contenthash].js',
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new ManifestPlugin()
  ]
}
